# qbgbaSQLData <img src='images/logo.png' align="right" height="139" />

This repo contains the complete SQL-dump of the Klinik-DB project in one zipped SQL-file. In this dump, you get the complete raw data of the German "Qualitaetsberichte der Krankenhaeuser" obtained from the "Gemeinsamer Bundesausschuss" (GBA) for the years 2015 until 2019.

## Usage

Set up your own MySQL / MariaDB database, download the zip file and import everything. The schema (name: "gbadata") is included in the dump file. 

## Beware of possible errors

We read, parsed and imported all data from the original XML-files obtained from the GBA. We did not alter anything to make sure anyone interested gets the raw data as it's reported to the GBA. All hospitals can be identified over time, and flaws in the original XML-files were corrected so that we were able to read in all files, but any weird data entries are completely due to erroneous data coming from the hospitals themself. 

If you see any errors, feel free to open up an issue in this repo to inform us and anyone else using this data about an error. If you spot any errors where the responsibility is on our side because our parsing code did not work correctly, please open an issue in the [qbgbaReader](https://gitlab.com/klinik-db/qbgbaReader/-/issues) repository so that we are able to fix them. PRs are welcome at any time! :-)

Thanks!

## Funding

Sponsored through the Prototype Fund by the German Federal Ministry of Education and Research from March 2021 to August 2021. 

<a href='https://klinik-db.de'><img src='images/BMBF_eng.png' align="left" height="139" /></a>
